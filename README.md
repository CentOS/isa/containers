# ISA SIG Containers

The ISA SIG containers are in this repository.

https://gitlab.com/CentOS/isa/containers/container_registry

## Using the Registry

Use these containers like you would any other containers.

```
podman run -it registry.gitlab.com/centos/isa/containers/isabaseline
podman run -it registry.gitlab.com/centos/isa/containers/isaoptimized
```
